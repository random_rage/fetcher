﻿namespace Fetcher
{
    partial class DBSettingsFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DBSettingsFrm));
            this.dbNameTB = new System.Windows.Forms.TextBox();
            this.dbNameLB = new System.Windows.Forms.Label();
            this.browseBtn = new System.Windows.Forms.Button();
            this.defaultDownloadDirTB = new System.Windows.Forms.TextBox();
            this.defaultDownloadDirLB = new System.Windows.Forms.Label();
            this.threadCountLB = new System.Windows.Forms.Label();
            this.threadCountNUD = new System.Windows.Forms.NumericUpDown();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.okBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.threadCountNUD)).BeginInit();
            this.SuspendLayout();
            // 
            // dbNameTB
            // 
            this.dbNameTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dbNameTB.Location = new System.Drawing.Point(12, 29);
            this.dbNameTB.Name = "dbNameTB";
            this.dbNameTB.Size = new System.Drawing.Size(438, 22);
            this.dbNameTB.TabIndex = 0;
            // 
            // dbNameLB
            // 
            this.dbNameLB.AutoSize = true;
            this.dbNameLB.Location = new System.Drawing.Point(12, 9);
            this.dbNameLB.Name = "dbNameLB";
            this.dbNameLB.Size = new System.Drawing.Size(112, 17);
            this.dbNameLB.TabIndex = 1;
            this.dbNameLB.Text = "Database name:";
            // 
            // browseBtn
            // 
            this.browseBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseBtn.Location = new System.Drawing.Point(375, 81);
            this.browseBtn.Name = "browseBtn";
            this.browseBtn.Size = new System.Drawing.Size(75, 25);
            this.browseBtn.TabIndex = 7;
            this.browseBtn.Text = "Browse...";
            this.browseBtn.UseVisualStyleBackColor = true;
            // 
            // defaultDownloadDirTB
            // 
            this.defaultDownloadDirTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.defaultDownloadDirTB.Location = new System.Drawing.Point(12, 83);
            this.defaultDownloadDirTB.Name = "defaultDownloadDirTB";
            this.defaultDownloadDirTB.Size = new System.Drawing.Size(361, 22);
            this.defaultDownloadDirTB.TabIndex = 6;
            // 
            // defaultDownloadDirLB
            // 
            this.defaultDownloadDirLB.AutoSize = true;
            this.defaultDownloadDirLB.Location = new System.Drawing.Point(12, 63);
            this.defaultDownloadDirLB.Name = "defaultDownloadDirLB";
            this.defaultDownloadDirLB.Size = new System.Drawing.Size(180, 17);
            this.defaultDownloadDirLB.TabIndex = 5;
            this.defaultDownloadDirLB.Text = "Default download directory:";
            // 
            // threadCountLB
            // 
            this.threadCountLB.AutoSize = true;
            this.threadCountLB.Location = new System.Drawing.Point(12, 122);
            this.threadCountLB.Name = "threadCountLB";
            this.threadCountLB.Size = new System.Drawing.Size(97, 17);
            this.threadCountLB.TabIndex = 8;
            this.threadCountLB.Text = "Thread count:";
            // 
            // threadCountNUD
            // 
            this.threadCountNUD.Location = new System.Drawing.Point(115, 122);
            this.threadCountNUD.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.threadCountNUD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.threadCountNUD.Name = "threadCountNUD";
            this.threadCountNUD.Size = new System.Drawing.Size(128, 22);
            this.threadCountNUD.TabIndex = 9;
            this.threadCountNUD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // cancelBtn
            // 
            this.cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cancelBtn.Location = new System.Drawing.Point(12, 161);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(100, 32);
            this.cancelBtn.TabIndex = 16;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // okBtn
            // 
            this.okBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okBtn.Location = new System.Drawing.Point(351, 161);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(100, 32);
            this.okBtn.TabIndex = 15;
            this.okBtn.Text = "Save";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // DBSettingsFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 205);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.threadCountNUD);
            this.Controls.Add(this.threadCountLB);
            this.Controls.Add(this.browseBtn);
            this.Controls.Add(this.defaultDownloadDirTB);
            this.Controls.Add(this.defaultDownloadDirLB);
            this.Controls.Add(this.dbNameLB);
            this.Controls.Add(this.dbNameTB);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DBSettingsFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Database settings";
            this.Shown += new System.EventHandler(this.DBSettingsFrm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.threadCountNUD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox dbNameTB;
        private System.Windows.Forms.Label dbNameLB;
        private System.Windows.Forms.Button browseBtn;
        private System.Windows.Forms.TextBox defaultDownloadDirTB;
        private System.Windows.Forms.Label defaultDownloadDirLB;
        private System.Windows.Forms.Label threadCountLB;
        private System.Windows.Forms.NumericUpDown threadCountNUD;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Button okBtn;
    }
}
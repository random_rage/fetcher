﻿using System;
using System.Windows.Forms;

namespace Fetcher
{
    public partial class DBSettingsFrm : Form
    {
        MainFrm mainFrm;

        public DBSettingsFrm()
        {
            InitializeComponent();
        }

        private void DBSettingsFrm_Shown(object sender, EventArgs e)
        {
            mainFrm = (MainFrm)Owner;
            dbNameTB.Text = mainFrm.Database.Name;
            defaultDownloadDirTB.Text = mainFrm.Database.DefaultDirectory;
            threadCountNUD.Value = mainFrm.Database.ThreadCount;
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            mainFrm.Database.Name = dbNameTB.Text;
            mainFrm.Database.DefaultDirectory = defaultDownloadDirTB.Text;
            mainFrm.Database.ThreadCount = (byte)threadCountNUD.Value;
            Close();
        }
    }
}

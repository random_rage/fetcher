﻿using System;
using System.Windows.Forms;

namespace Fetcher
{
    partial class FetchItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Awesomium.Core.WebPreferences webPreferences3 = new Awesomium.Core.WebPreferences(true);
            this.webBrowser = new Awesomium.Windows.Forms.WebControl(this.components);
            this.webSessionProvider = new Awesomium.Windows.Forms.WebSessionProvider(this.components);
            this.SuspendLayout();
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(0, 0);
            this.webBrowser.Size = new System.Drawing.Size(622, 275);
            this.webBrowser.TabIndex = 1;
            // 
            // webSessionProvider
            // 
            webPreferences3.AppCache = false;
            webPreferences3.JavascriptViewChangeSource = false;
            webPreferences3.JavascriptViewEvents = false;
            webPreferences3.JavascriptViewExecute = false;
            webPreferences3.LoadImagesAutomatically = false;
            webPreferences3.LocalStorage = false;
            webPreferences3.PdfJS = false;
            webPreferences3.Plugins = false;
            webPreferences3.RemoteFonts = false;
            webPreferences3.ShrinkStandaloneImagesToFit = false;
            webPreferences3.WebAudio = false;
            this.webSessionProvider.Preferences = webPreferences3;
            this.webSessionProvider.Views.Add(this.webBrowser);
            // 
            // FetchItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 275);
            this.Controls.Add(this.webBrowser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FetchItem";
            this.ShowIcon = false;
            this.Text = "Spider is searching the direct download link... ";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.ResumeLayout(false);

        }

        #endregion

        private Awesomium.Windows.Forms.WebControl webBrowser;
        private Awesomium.Windows.Forms.WebSessionProvider webSessionProvider;
    }
}
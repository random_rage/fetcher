﻿using Awesomium.Core;
using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Fetcher
{
    public enum FetchItemState
    {
        /// <summary>
        /// The item has not been initialized
        /// </summary>
        NotInitialized,
        /// <summary>
        /// Initialization process started
        /// </summary>
        Initializing,
        /// <summary>
        /// Item initialized, searching direct download link
        /// </summary>
        Ready,
        /// <summary>
        /// Download process started
        /// </summary>
        Downloading,
        /// <summary>
        /// Download process ended successfully
        /// </summary>
        Done,
        /// <summary>
        /// Download process aborting
        /// </summary>
        Stopping,
        /// <summary>
        /// Download process aborted
        /// </summary>
        Stopped,
        /// <summary>
        /// Error occurred
        /// </summary>
        Error
    }

    /// <summary>
    /// Represents a class that describes an item
    /// </summary>
    public partial class FetchItem : Form
    {
        #region Private fields
        private string _itemName, _dir, _downloadLink, _category, _jsFile;
        private bool /*_torrent, */_useJS;
        private FetchItemState _state;

        private DownloadItem _downloadInfo;
        #endregion

        #region Public properties
        /// <summary>
        /// Item name
        /// </summary>
        public string ItemName
        {
            get { return _itemName; }
            set { _itemName = value; }
        }

        /// <summary>
        /// File directory on a local storage
        /// </summary>
        public string Directory
        {
            get { return _dir; }
            set
            {
                if (value.IndexOfAny(System.IO.Path.GetInvalidPathChars()) < 0)
                    _dir = value;
            }
        }

        /// <summary>
        /// Direct OR Indirect download link to the item
        /// </summary>
        public string DownloadLink
        {
            get { return _downloadLink; }
            set
            {
                if (System.Uri.IsWellFormedUriString(value, UriKind.Absolute))
                    _downloadLink = value;
            }
        }

        /// <summary>
        /// Item category in a item base
        /// </summary>
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        /// <summary>
        /// Regular expression that contains "Target" group. Need to retrieve Uri (direct link) to the item
        /// </summary>
        public string JavaScriptFile
        {
            get { return _jsFile; }
            set { _jsFile = value; }
        }

        /// <summary>
        /// Is it a torrent file to download?
        /// </summary>
        /*public bool Torrent
        {
            get { return _torrent; }
            set { _torrent = value; }
        }*/

        /// <summary>
        /// Use JavaScript if need to get direct download link manually
        /// </summary>
        public bool UseJS
        {
            get { return _useJS; }
            set { _useJS = value; }
        }

        /// <summary>
        /// Item state
        /// </summary>
        public FetchItemState State
        {
            get { return _state; }
        }

        /// <summary>
        /// Information about downloading process
        /// </summary>
        public DownloadItem DownloadInfo
        {
            get { return _downloadInfo; }
        }

        /// <summary>
        /// String representation of the item state
        /// </summary>
        public string StateString
        {
            get
            {
                switch (_state)
                {
                    case FetchItemState.NotInitialized:
                        return "Not initialized";
                    case FetchItemState.Initializing:
                        return "Initializing";
                    case FetchItemState.Ready:
                        return "Ready, waiting for a direct link";
                    case FetchItemState.Downloading:
                        return "Downloading";
                    case FetchItemState.Done:
                        return "Done";
                    case FetchItemState.Stopping:
                        return "Stopping";
                    case FetchItemState.Stopped:
                        return "Stopped";
                    case FetchItemState.Error:
                        return "Error";
                    default:
                        return "Unknow";
                }
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor. Set up properties before use!
        /// </summary>
        /// <param name="type">Type of the item (file or text)</param>
        public FetchItem()
        {
            InitializeComponent();

            _itemName = string.Empty;
            _dir = string.Empty;
            _downloadLink = string.Empty;
            _category = string.Empty;
            _jsFile = string.Empty;
            //_torrent = false;
            _useJS = false;

            _state = FetchItemState.NotInitialized;

            _downloadInfo = null;

            WebCore.Download += WebCore_Download;
            WebCore.DownloadBegin += WebCore_DownloadBegin;
        }

        /// <summary>
        /// Constructor from serialized string
        /// </summary>
        /// <param name="serialized">Serialized string</param>
        public FetchItem(string serialized, char serializationSeparator)
        {
            InitializeComponent();

            string[] deserialized = serialized.Split(serializationSeparator);
            _itemName = deserialized[0];
            _dir = deserialized[1];
            _downloadLink = deserialized[2];
            _category = deserialized[3];
            _useJS = bool.Parse(deserialized[4]);
            _jsFile = deserialized[5];
            //_torrent = bool.Parse(deserialized[6]);

            _state = FetchItemState.NotInitialized;

            _downloadInfo = null;

            WebCore.Download += WebCore_Download;
            WebCore.DownloadBegin += WebCore_DownloadBegin;
        }
        #endregion

        #region Private methods
        private void WebCore_Download(object sender, DownloadEventArgs e)
        {
            if (e.ViewId == webBrowser.Identifier)
            {
                e.Handled = true;
                e.SelectedFile = System.IO.Path.Combine(_dir, e.SuggestedFileName);
            }
        }

        private void WebCore_DownloadBegin(object sender, DownloadBeginEventArgs e)
        {
            if (e.Info.OriginViewId == webBrowser.Identifier)
            {
                _downloadInfo = e.Info;
                _state = FetchItemState.Downloading;

                _downloadInfo.Canceling += downloadInfo_Canceling;
                _downloadInfo.Canceled += downloadInfo_Canceled;
                _downloadInfo.Completed += downloadInfo_Completed;
            }
        }

        private void downloadInfo_Canceling(object sender, CancelEventArgs e)
        {
            _state = FetchItemState.Stopping;
        }

        private void downloadInfo_Completed(object sender, EventArgs e)
        {
            _state = FetchItemState.Done;
            Close();
        }

        private void downloadInfo_Canceled(object sender, EventArgs e)
        {
            _state = FetchItemState.Stopped;
            Close();
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Starts downloading from the URI. WARNING!!! Check IsDisposed before using it!
        /// </summary>
        public void StartDownload()
        {
            _state = FetchItemState.Initializing;

            try
            {
                // Trying to create target directory if it doesn't exist
                if (!System.IO.Directory.Exists(_dir))
                    System.IO.Directory.CreateDirectory(_dir);

                // Trying to load the web page
                webBrowser.Source = new Uri(_downloadLink);

                // Trying to open and hide the form
                Show();
                Hide();
            }
            catch (Exception ex)
            {
                Program.ShowError(ex.ToString(), "Start download error");
                _state = FetchItemState.Error;
                return;
            }

            int waitAttempts = 0;

            while (!webBrowser.IsDocumentReady && waitAttempts < Program.WEB_SPIDER_DOM_WAITING_MAX_ATTEMPTS)
            {
                Application.DoEvents();
                Thread.Sleep(Program.WEB_SPIDER_DOM_WAITING_TIMEOUT);
                waitAttempts++;
            }

            if (_useJS && _jsFile != string.Empty)
            {
                if (!File.Exists(_jsFile))
                {
                    Program.ShowError("JavaScript file " + _jsFile + " not found!", "Execute JavaScript error");
                    _state = FetchItemState.Error;
                    return;
                }

                string js = File.ReadAllText(_jsFile, Encoding.UTF8);
                bool executed = false;

                while (!executed)
                {
                    try
                    {
                        if (!webBrowser.IsDocumentReady)
                            Application.DoEvents();

                        webBrowser.ExecuteJavascript(js);
                        executed = true;
                    }
                    catch (InvalidOperationException)
                    {
                        switch (MessageBox.Show("Error executing javascript \"" + _jsFile + 
                                        "\"! Retry?", "Item initialization error",
                                        MessageBoxButtons.AbortRetryIgnore, 
                                        MessageBoxIcon.Error))
                        {
                            case DialogResult.Ignore:
                                executed = true;
                                break;
                            case DialogResult.Retry:
                                executed = false;
                                break;
                            default:
                                executed = false;
                                _state = FetchItemState.Error;
                                return;
                        }
                    }
                }
            }

            if (_state == FetchItemState.Initializing)
                _state = FetchItemState.Ready;
        }

        /// <summary>
        /// Stops downloading from the URI
        /// </summary>
        public void StopDownloading()
        {
            if (_downloadInfo != null)
                _downloadInfo.Cancel();
        }

        /// <summary>
        /// Returns the item that serialized to a string
        /// </summary>
        /// <returns>Serialized item</returns>
        public string ToString(char serializationSeparator)
        {
            return string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}",
                                 serializationSeparator,
                                 ItemName,
                                 Directory,
                                 DownloadLink,
                                 Category,
                                 UseJS,
                                 JavaScriptFile);
            /*return string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}",
                                 serializationSeparator,
                                 ItemName,
                                 Directory,
                                 DownloadLink,
                                 Category,
                                 UseJS,
                                 JavaScriptFile,
                                 Torrent);*/
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.IO;
using System.Text;

namespace Fetcher
{
    public class FetchItemCollection : CollectionBase
    {
        #region Private fields
        private string _name, _defaultDir;
        private byte _threadCount;
        #endregion

        #region Public properties
        /// <summary>
        /// Collection (database) name
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Default download directory for new items
        /// </summary>
        public string DefaultDirectory
        {
            get { return _defaultDir; }
            set
            {
                if (value.IndexOfAny(System.IO.Path.GetInvalidPathChars()) < 0)
                    _defaultDir = value;
            }
        } 

        /// <summary>
        /// Download threads count
        /// </summary>
        public byte ThreadCount
        {
            get { return _threadCount; }
            set { _threadCount = value; }
        }
        #endregion

        #region Public methods
        public FetchItem this[int index]
        {
            get
            {
                return ((FetchItem)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        /// <summary>
        /// Adds an item to the collection
        /// </summary>
        /// <param name="value">Item</param>
        /// <returns>Returns index in the collection</returns>
        public int Add(FetchItem value)
        {
            return (List.Add(value));
        }

        public int IndexOf(FetchItem value)
        {
            return (List.IndexOf(value));
        }

        public void Insert(int index, FetchItem value)
        {
            List.Insert(index, value);
        }

        public void Remove(FetchItem value)
        {
            List.Remove(value);
        }

        public bool Contains(FetchItem value)
        {
            // If value is not of type FetchItem, this will return false.
            return (List.Contains(value));
        }

        /// <summary>
        /// Checks if fetch item with "downloadLink" exists in collection
        /// </summary>
        /// <param name="downloadLink">Indirect (or direct) download link</param>
        /// <returns>True if item exists, otherwise - False</returns>
        public bool Contains(string downloadLink)
        {
            foreach (FetchItem fetchItem in List)
            {
                if (fetchItem.DownloadLink == downloadLink)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Saves the collection to a file
        /// </summary>
        /// <param name="fileName">Target file name</param>
        public void Save(string fileName)
        {
            lock (List)
            {
                string[] db = new string[Count + 1];

                db[0] = string.Format(
                                        "{1}{0}{2}{0}{3}", 
                                        Program.SERIALIZATION_SEPARATOR, 
                                        Name, 
                                        DefaultDirectory, 
                                        ThreadCount
                                     );

                for (int i = 0; i < Count; i++)
                    db[i + 1] = this[i].ToString(Program.SERIALIZATION_SEPARATOR);

                try
                {
                    File.WriteAllLines(fileName, db, Encoding.UTF8);
                }
                catch (Exception ex)
                {
                    Program.ShowError(ex.ToString(), "Error saving database");
                }
            }
        }

        /// <summary>
        /// Loads the collection from a file
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <returns></returns>
        public void Load(string fileName)
        {
            lock (List)
            {
                try
                {
                    string[] db = File.ReadAllLines(fileName, Encoding.UTF8);

                    string[] opts = db[0].Split(Program.SERIALIZATION_SEPARATOR);
                    Name = opts[0];
                    DefaultDirectory = opts[1];
                    ThreadCount = byte.Parse(opts[2]);

                    for (int i = 1; i < db.Length; i++)
                        Add(new FetchItem(db[i], Program.SERIALIZATION_SEPARATOR));
                }
                catch (Exception ex)
                {
                    Program.ShowError(ex.ToString(), "Error loading database");
                }
            }
        }
        #endregion

        protected override void OnValidate(Object value)
        {
            if (value.GetType() != typeof(FetchItem))
                throw new ArgumentException("value must be of type FetchItem.", "value");
        }
    }
}

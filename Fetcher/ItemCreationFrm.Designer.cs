﻿namespace Fetcher
{
    partial class ItemCreationFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemCreationFrm));
            this.itemNameLB = new System.Windows.Forms.Label();
            this.itemNameTB = new System.Windows.Forms.TextBox();
            this.downloadDirTB = new System.Windows.Forms.TextBox();
            this.downloadDirLB = new System.Windows.Forms.Label();
            this.downloadDirBrowseBtn = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.downloadLinkTB = new System.Windows.Forms.TextBox();
            this.downloadLinkLB = new System.Windows.Forms.Label();
            this.categoryLB = new System.Windows.Forms.Label();
            this.categoryComboBox = new System.Windows.Forms.ComboBox();
            this.createBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.useJSCB = new System.Windows.Forms.CheckBox();
            this.jsFileBrowseBtn = new System.Windows.Forms.Button();
            this.jsFileTB = new System.Windows.Forms.TextBox();
            this.jsFileLB = new System.Windows.Forms.Label();
            this.torrentCB = new System.Windows.Forms.CheckBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // itemNameLB
            // 
            this.itemNameLB.AutoSize = true;
            this.itemNameLB.Location = new System.Drawing.Point(12, 9);
            this.itemNameLB.Name = "itemNameLB";
            this.itemNameLB.Size = new System.Drawing.Size(77, 17);
            this.itemNameLB.TabIndex = 0;
            this.itemNameLB.Text = "Item name:";
            // 
            // itemNameTB
            // 
            this.itemNameTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.itemNameTB.Location = new System.Drawing.Point(12, 29);
            this.itemNameTB.Name = "itemNameTB";
            this.itemNameTB.Size = new System.Drawing.Size(438, 22);
            this.itemNameTB.TabIndex = 1;
            // 
            // downloadDirTB
            // 
            this.downloadDirTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.downloadDirTB.Location = new System.Drawing.Point(12, 134);
            this.downloadDirTB.Name = "downloadDirTB";
            this.downloadDirTB.Size = new System.Drawing.Size(361, 22);
            this.downloadDirTB.TabIndex = 3;
            // 
            // downloadDirLB
            // 
            this.downloadDirLB.AutoSize = true;
            this.downloadDirLB.Location = new System.Drawing.Point(12, 114);
            this.downloadDirLB.Name = "downloadDirLB";
            this.downloadDirLB.Size = new System.Drawing.Size(133, 17);
            this.downloadDirLB.TabIndex = 2;
            this.downloadDirLB.Text = "Download directory:";
            // 
            // downloadDirBrowseBtn
            // 
            this.downloadDirBrowseBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.downloadDirBrowseBtn.Location = new System.Drawing.Point(375, 132);
            this.downloadDirBrowseBtn.Name = "downloadDirBrowseBtn";
            this.downloadDirBrowseBtn.Size = new System.Drawing.Size(75, 25);
            this.downloadDirBrowseBtn.TabIndex = 4;
            this.downloadDirBrowseBtn.Text = "Browse...";
            this.downloadDirBrowseBtn.UseVisualStyleBackColor = true;
            this.downloadDirBrowseBtn.Click += new System.EventHandler(this.downloadDirBrowseBtn_Click);
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.Description = "Choose download directory for an item";
            // 
            // downloadLinkTB
            // 
            this.downloadLinkTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.downloadLinkTB.Location = new System.Drawing.Point(12, 190);
            this.downloadLinkTB.Name = "downloadLinkTB";
            this.downloadLinkTB.Size = new System.Drawing.Size(438, 22);
            this.downloadLinkTB.TabIndex = 6;
            // 
            // downloadLinkLB
            // 
            this.downloadLinkLB.AutoSize = true;
            this.downloadLinkLB.Location = new System.Drawing.Point(12, 170);
            this.downloadLinkLB.Name = "downloadLinkLB";
            this.downloadLinkLB.Size = new System.Drawing.Size(99, 17);
            this.downloadLinkLB.TabIndex = 5;
            this.downloadLinkLB.Text = "Download link:";
            // 
            // categoryLB
            // 
            this.categoryLB.AutoSize = true;
            this.categoryLB.Location = new System.Drawing.Point(12, 59);
            this.categoryLB.Name = "categoryLB";
            this.categoryLB.Size = new System.Drawing.Size(69, 17);
            this.categoryLB.TabIndex = 7;
            this.categoryLB.Text = "Category:";
            // 
            // categoryComboBox
            // 
            this.categoryComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.categoryComboBox.FormattingEnabled = true;
            this.categoryComboBox.Location = new System.Drawing.Point(12, 79);
            this.categoryComboBox.Name = "categoryComboBox";
            this.categoryComboBox.Size = new System.Drawing.Size(438, 24);
            this.categoryComboBox.TabIndex = 8;
            // 
            // createBtn
            // 
            this.createBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.createBtn.Location = new System.Drawing.Point(350, 331);
            this.createBtn.Name = "createBtn";
            this.createBtn.Size = new System.Drawing.Size(100, 32);
            this.createBtn.TabIndex = 13;
            this.createBtn.Text = "Create!";
            this.createBtn.UseVisualStyleBackColor = true;
            this.createBtn.Click += new System.EventHandler(this.createBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cancelBtn.Location = new System.Drawing.Point(11, 331);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(100, 32);
            this.cancelBtn.TabIndex = 14;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // useJSCB
            // 
            this.useJSCB.AutoSize = true;
            this.useJSCB.Location = new System.Drawing.Point(15, 231);
            this.useJSCB.Name = "useJSCB";
            this.useJSCB.Size = new System.Drawing.Size(147, 21);
            this.useJSCB.TabIndex = 17;
            this.useJSCB.Text = "Use JavaScript file";
            this.useJSCB.UseVisualStyleBackColor = true;
            this.useJSCB.CheckedChanged += new System.EventHandler(this.useJSCB_CheckedChanged);
            // 
            // jsFileBrowseBtn
            // 
            this.jsFileBrowseBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.jsFileBrowseBtn.Location = new System.Drawing.Point(375, 283);
            this.jsFileBrowseBtn.Name = "jsFileBrowseBtn";
            this.jsFileBrowseBtn.Size = new System.Drawing.Size(75, 25);
            this.jsFileBrowseBtn.TabIndex = 20;
            this.jsFileBrowseBtn.Text = "Browse...";
            this.jsFileBrowseBtn.UseVisualStyleBackColor = true;
            this.jsFileBrowseBtn.Visible = false;
            this.jsFileBrowseBtn.Click += new System.EventHandler(this.jsFileBrowseBtn_Click);
            // 
            // jsFileTB
            // 
            this.jsFileTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.jsFileTB.Location = new System.Drawing.Point(12, 285);
            this.jsFileTB.Name = "jsFileTB";
            this.jsFileTB.Size = new System.Drawing.Size(361, 22);
            this.jsFileTB.TabIndex = 19;
            this.jsFileTB.Visible = false;
            // 
            // jsFileLB
            // 
            this.jsFileLB.AutoSize = true;
            this.jsFileLB.Location = new System.Drawing.Point(12, 265);
            this.jsFileLB.Name = "jsFileLB";
            this.jsFileLB.Size = new System.Drawing.Size(100, 17);
            this.jsFileLB.TabIndex = 18;
            this.jsFileLB.Text = "JavaScript file:";
            this.jsFileLB.Visible = false;
            // 
            // torrentCB
            // 
            this.torrentCB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.torrentCB.AutoSize = true;
            this.torrentCB.Enabled = false;
            this.torrentCB.Location = new System.Drawing.Point(345, 231);
            this.torrentCB.Name = "torrentCB";
            this.torrentCB.Size = new System.Drawing.Size(105, 21);
            this.torrentCB.TabIndex = 21;
            this.torrentCB.Text = "It\'s a torrent";
            this.torrentCB.UseVisualStyleBackColor = true;
            this.torrentCB.Visible = false;
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "js";
            this.openFileDialog.FileName = "download.js";
            this.openFileDialog.Filter = "JavaScript files|*.js|Plain text|*.txt|All files|*.*";
            this.openFileDialog.SupportMultiDottedExtensions = true;
            this.openFileDialog.Title = "Choose JS file which will be executed to get a direct download link";
            // 
            // ItemCreationFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 375);
            this.Controls.Add(this.torrentCB);
            this.Controls.Add(this.jsFileBrowseBtn);
            this.Controls.Add(this.jsFileTB);
            this.Controls.Add(this.jsFileLB);
            this.Controls.Add(this.useJSCB);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.createBtn);
            this.Controls.Add(this.categoryComboBox);
            this.Controls.Add(this.categoryLB);
            this.Controls.Add(this.downloadLinkTB);
            this.Controls.Add(this.downloadLinkLB);
            this.Controls.Add(this.downloadDirBrowseBtn);
            this.Controls.Add(this.downloadDirTB);
            this.Controls.Add(this.downloadDirLB);
            this.Controls.Add(this.itemNameTB);
            this.Controls.Add(this.itemNameLB);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(480, 420);
            this.Name = "ItemCreationFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create new fetch item";
            this.Shown += new System.EventHandler(this.ItemCreationFrm_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label itemNameLB;
        private System.Windows.Forms.TextBox itemNameTB;
        private System.Windows.Forms.TextBox downloadDirTB;
        private System.Windows.Forms.Label downloadDirLB;
        private System.Windows.Forms.Button downloadDirBrowseBtn;
        protected internal System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.TextBox downloadLinkTB;
        private System.Windows.Forms.Label downloadLinkLB;
        private System.Windows.Forms.Label categoryLB;
        private System.Windows.Forms.ComboBox categoryComboBox;
        private System.Windows.Forms.Button createBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.CheckBox useJSCB;
        private System.Windows.Forms.Button jsFileBrowseBtn;
        private System.Windows.Forms.TextBox jsFileTB;
        private System.Windows.Forms.Label jsFileLB;
        private System.Windows.Forms.CheckBox torrentCB;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}
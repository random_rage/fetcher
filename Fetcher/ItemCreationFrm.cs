﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Fetcher
{
    public partial class ItemCreationFrm : Form
    {
        MainFrm mainFrm;

        public ItemCreationFrm()
        {
            InitializeComponent();
        }

        private void ItemCreationFrm_Shown(object sender, EventArgs e)
        {
            mainFrm = (MainFrm)Owner;
            downloadDirTB.Text = mainFrm.Database.DefaultDirectory;

            categoryComboBox.Items.Clear();
            foreach (ListViewGroup cat in mainFrm.itemListLV.Groups)
                categoryComboBox.Items.Add(cat.Header);
        }

        #region GUI
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void createBtn_Click(object sender, EventArgs e)
        {
            // Checking name
            if (itemNameTB.TextLength < 1)
            {
                Program.ShowError("Please, choose the item name!", "Item creation error");
                return;
            }

            // Checking download directory path
            if (downloadDirTB.TextLength < 1 || downloadDirTB.Text.IndexOfAny(Path.GetInvalidPathChars()) > -1)
            {
                Program.ShowError("Please, enter the correct download directory path!", "Item creation error");
                return;
            }

            // Checking download link
            if (downloadLinkTB.TextLength < 1 || !Uri.IsWellFormedUriString(downloadLinkTB.Text, UriKind.Absolute))
            {
                Program.ShowError("Please, enter the correct download link!", "Item creation error");
                return;
            }

            if (mainFrm.Database.Contains(downloadLinkTB.Text))
            {
                Program.ShowError("Fetch item with same download link has already exist in database!", "Item creation error");
                return;
            }

            // Checking JS file existence
            if (useJSCB.Checked && (jsFileTB.TextLength < 1 || !File.Exists(jsFileTB.Text)))
            {
                Program.ShowError(
                                    string.Format("File {0} doesn't exist!", jsFileTB.Text),
                                    "Item creation error"
                                 );
                return;
            }

            FetchItem fetchItem = new FetchItem()
            {
                ItemName = itemNameTB.Text,
                Category = categoryComboBox.Text,
                Directory = downloadDirTB.Text,
                DownloadLink = downloadLinkTB.Text,
                UseJS = useJSCB.Checked,
                //Torrent = torrentCB.Checked,
                JavaScriptFile = jsFileTB.Text
            };

            mainFrm.Database.Add(fetchItem);
            mainFrm.AddItemToListView(fetchItem).Checked = true;
            Close();
        }

        private void downloadDirBrowseBtn_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                downloadDirTB.Text = folderBrowserDialog.SelectedPath;
        }

        private void jsFileBrowseBtn_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
                jsFileTB.Text = openFileDialog.FileName;
        }

        private void useJSCB_CheckedChanged(object sender, EventArgs e)
        {
            jsFileLB.Visible = jsFileTB.Visible = jsFileBrowseBtn.Visible = useJSCB.Checked;
        } 
        #endregion
    }
}

﻿namespace Fetcher
{
    partial class MainFrm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrm));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.dataBaseTSDDB = new System.Windows.Forms.ToolStripDropDownButton();
            this.openDatabaseTSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.saveDatabaseTSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.closeDatabaseTSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.settingsDatabaseTSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.itemsTSDDB = new System.Windows.Forms.ToolStripDropDownButton();
            this.addTSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.removeTSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.downloadTSSB = new System.Windows.Forms.ToolStripSplitButton();
            this.downloadAllTSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.downloadAgainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.clearAllDownloadsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopDownloadingTSSB = new System.Windows.Forms.ToolStripSplitButton();
            this.stopAllTSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.statusTSSL = new System.Windows.Forms.ToolStripStatusLabel();
            this.progressTSPB = new System.Windows.Forms.ToolStripProgressBar();
            this.itemListLV = new System.Windows.Forms.ListView();
            this.idCH = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.nameCH = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.stateCH = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.progressCH = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.speedCH = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dirCH = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fileCH = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.uriCH = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.downloadLinkCH = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.itemListCMS = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openTSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.browseDirectoryTSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.checkAllTSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.uncheckAllTSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.updateTmr = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.helpTSDDB = new System.Windows.Forms.ToolStripDropDownButton();
            this.aboutTSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.helpTSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.statusStrip.SuspendLayout();
            this.itemListCMS.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpTSDDB,
            this.dataBaseTSDDB,
            this.itemsTSDDB,
            this.downloadTSSB,
            this.stopDownloadingTSSB,
            this.statusTSSL,
            this.progressTSPB});
            this.statusStrip.Location = new System.Drawing.Point(0, 529);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.ShowItemToolTips = true;
            this.statusStrip.Size = new System.Drawing.Size(1006, 26);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "Status strip";
            // 
            // dataBaseTSDDB
            // 
            this.dataBaseTSDDB.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openDatabaseTSMI,
            this.saveDatabaseTSMI,
            this.toolStripSeparator2,
            this.closeDatabaseTSMI,
            this.toolStripSeparator1,
            this.settingsDatabaseTSMI});
            this.dataBaseTSDDB.Image = ((System.Drawing.Image)(resources.GetObject("dataBaseTSDDB.Image")));
            this.dataBaseTSDDB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.dataBaseTSDDB.Name = "dataBaseTSDDB";
            this.dataBaseTSDDB.Size = new System.Drawing.Size(106, 24);
            this.dataBaseTSDDB.Text = "Database";
            this.dataBaseTSDDB.ToolTipText = "Database menu";
            // 
            // openDatabaseTSMI
            // 
            this.openDatabaseTSMI.Image = ((System.Drawing.Image)(resources.GetObject("openDatabaseTSMI.Image")));
            this.openDatabaseTSMI.Name = "openDatabaseTSMI";
            this.openDatabaseTSMI.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openDatabaseTSMI.Size = new System.Drawing.Size(181, 26);
            this.openDatabaseTSMI.Text = "Open";
            this.openDatabaseTSMI.ToolTipText = "Open a database";
            this.openDatabaseTSMI.Click += new System.EventHandler(this.openDatabaseTSMI_Click);
            // 
            // saveDatabaseTSMI
            // 
            this.saveDatabaseTSMI.Image = ((System.Drawing.Image)(resources.GetObject("saveDatabaseTSMI.Image")));
            this.saveDatabaseTSMI.Name = "saveDatabaseTSMI";
            this.saveDatabaseTSMI.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveDatabaseTSMI.Size = new System.Drawing.Size(181, 26);
            this.saveDatabaseTSMI.Text = "Save";
            this.saveDatabaseTSMI.Click += new System.EventHandler(this.saveDatabaseTSMI_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(178, 6);
            // 
            // closeDatabaseTSMI
            // 
            this.closeDatabaseTSMI.Image = ((System.Drawing.Image)(resources.GetObject("closeDatabaseTSMI.Image")));
            this.closeDatabaseTSMI.Name = "closeDatabaseTSMI";
            this.closeDatabaseTSMI.Size = new System.Drawing.Size(181, 26);
            this.closeDatabaseTSMI.Text = "Close";
            this.closeDatabaseTSMI.ToolTipText = "Close current database";
            this.closeDatabaseTSMI.Click += new System.EventHandler(this.closeDatabaseTSMI_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(178, 6);
            // 
            // settingsDatabaseTSMI
            // 
            this.settingsDatabaseTSMI.Image = ((System.Drawing.Image)(resources.GetObject("settingsDatabaseTSMI.Image")));
            this.settingsDatabaseTSMI.Name = "settingsDatabaseTSMI";
            this.settingsDatabaseTSMI.Size = new System.Drawing.Size(181, 26);
            this.settingsDatabaseTSMI.Text = "Settings";
            this.settingsDatabaseTSMI.Click += new System.EventHandler(this.settingsDatabaseTSMI_Click);
            // 
            // itemsTSDDB
            // 
            this.itemsTSDDB.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTSMI,
            this.removeTSMI});
            this.itemsTSDDB.Image = ((System.Drawing.Image)(resources.GetObject("itemsTSDDB.Image")));
            this.itemsTSDDB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.itemsTSDDB.Margin = new System.Windows.Forms.Padding(4, 2, 0, 0);
            this.itemsTSDDB.Name = "itemsTSDDB";
            this.itemsTSDDB.Size = new System.Drawing.Size(79, 24);
            this.itemsTSDDB.Text = "Items";
            this.itemsTSDDB.DropDownOpening += new System.EventHandler(this.itemsTSDDB_DropDownOpening);
            // 
            // addTSMI
            // 
            this.addTSMI.Image = ((System.Drawing.Image)(resources.GetObject("addTSMI.Image")));
            this.addTSMI.Name = "addTSMI";
            this.addTSMI.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.addTSMI.Size = new System.Drawing.Size(181, 26);
            this.addTSMI.Text = "Add";
            this.addTSMI.ToolTipText = "Create and add new item";
            this.addTSMI.Click += new System.EventHandler(this.addTSMI_Click);
            // 
            // removeTSMI
            // 
            this.removeTSMI.Image = ((System.Drawing.Image)(resources.GetObject("removeTSMI.Image")));
            this.removeTSMI.Name = "removeTSMI";
            this.removeTSMI.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.removeTSMI.Size = new System.Drawing.Size(181, 26);
            this.removeTSMI.Text = "Remove";
            this.removeTSMI.ToolTipText = "Remove selected item(s)";
            this.removeTSMI.Click += new System.EventHandler(this.removeTSMI_Click);
            // 
            // downloadTSSB
            // 
            this.downloadTSSB.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.downloadAllTSMI,
            this.toolStripSeparator4,
            this.downloadAgainToolStripMenuItem,
            this.toolStripSeparator3,
            this.clearAllDownloadsToolStripMenuItem});
            this.downloadTSSB.Image = ((System.Drawing.Image)(resources.GetObject("downloadTSSB.Image")));
            this.downloadTSSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.downloadTSSB.Margin = new System.Windows.Forms.Padding(4, 2, 0, 0);
            this.downloadTSSB.Name = "downloadTSSB";
            this.downloadTSSB.Size = new System.Drawing.Size(117, 24);
            this.downloadTSSB.Text = "Download";
            this.downloadTSSB.ToolTipText = "Download current item";
            this.downloadTSSB.ButtonClick += new System.EventHandler(this.downloadTSSB_ButtonClick);
            this.downloadTSSB.DropDownOpening += new System.EventHandler(this.downloadTSSB_DropDownOpening);
            // 
            // downloadAllTSMI
            // 
            this.downloadAllTSMI.Image = ((System.Drawing.Image)(resources.GetObject("downloadAllTSMI.Image")));
            this.downloadAllTSMI.Name = "downloadAllTSMI";
            this.downloadAllTSMI.Size = new System.Drawing.Size(215, 26);
            this.downloadAllTSMI.Text = "Download all";
            this.downloadAllTSMI.ToolTipText = "Download all items in DB";
            this.downloadAllTSMI.Click += new System.EventHandler(this.downloadAllTSMI_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(212, 6);
            // 
            // downloadAgainToolStripMenuItem
            // 
            this.downloadAgainToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("downloadAgainToolStripMenuItem.Image")));
            this.downloadAgainToolStripMenuItem.Name = "downloadAgainToolStripMenuItem";
            this.downloadAgainToolStripMenuItem.Size = new System.Drawing.Size(215, 26);
            this.downloadAgainToolStripMenuItem.Text = "Download again";
            this.downloadAgainToolStripMenuItem.Click += new System.EventHandler(this.downloadAgainToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(212, 6);
            // 
            // clearAllDownloadsToolStripMenuItem
            // 
            this.clearAllDownloadsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("clearAllDownloadsToolStripMenuItem.Image")));
            this.clearAllDownloadsToolStripMenuItem.Name = "clearAllDownloadsToolStripMenuItem";
            this.clearAllDownloadsToolStripMenuItem.Size = new System.Drawing.Size(215, 26);
            this.clearAllDownloadsToolStripMenuItem.Text = "Clear all downloads";
            this.clearAllDownloadsToolStripMenuItem.Click += new System.EventHandler(this.clearAllDownloadsToolStripMenuItem_Click);
            // 
            // stopDownloadingTSSB
            // 
            this.stopDownloadingTSSB.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stopAllTSMI});
            this.stopDownloadingTSSB.Image = ((System.Drawing.Image)(resources.GetObject("stopDownloadingTSSB.Image")));
            this.stopDownloadingTSSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stopDownloadingTSSB.Margin = new System.Windows.Forms.Padding(4, 2, 0, 0);
            this.stopDownloadingTSSB.Name = "stopDownloadingTSSB";
            this.stopDownloadingTSSB.Size = new System.Drawing.Size(171, 24);
            this.stopDownloadingTSSB.Text = "Stop downloading";
            this.stopDownloadingTSSB.Visible = false;
            this.stopDownloadingTSSB.ButtonClick += new System.EventHandler(this.stopDownloadingTSSB_ButtonClick);
            // 
            // stopAllTSMI
            // 
            this.stopAllTSMI.Name = "stopAllTSMI";
            this.stopAllTSMI.Size = new System.Drawing.Size(181, 26);
            this.stopAllTSMI.Text = "Stop all";
            this.stopAllTSMI.Click += new System.EventHandler(this.stopAllTSMI_Click);
            // 
            // statusTSSL
            // 
            this.statusTSSL.Margin = new System.Windows.Forms.Padding(4, 3, 0, 2);
            this.statusTSSL.Name = "statusTSSL";
            this.statusTSSL.Size = new System.Drawing.Size(324, 21);
            this.statusTSSL.Spring = true;
            this.statusTSSL.Text = "Ready";
            this.statusTSSL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.statusTSSL.ToolTipText = "Status label";
            // 
            // progressTSPB
            // 
            this.progressTSPB.Margin = new System.Windows.Forms.Padding(4, 3, 1, 3);
            this.progressTSPB.Name = "progressTSPB";
            this.progressTSPB.Size = new System.Drawing.Size(100, 20);
            this.progressTSPB.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressTSPB.ToolTipText = "Download progress";
            // 
            // itemListLV
            // 
            this.itemListLV.AllowColumnReorder = true;
            this.itemListLV.AllowDrop = true;
            this.itemListLV.CheckBoxes = true;
            this.itemListLV.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.idCH,
            this.nameCH,
            this.stateCH,
            this.progressCH,
            this.speedCH,
            this.dirCH,
            this.fileCH,
            this.uriCH,
            this.downloadLinkCH});
            this.itemListLV.ContextMenuStrip = this.itemListCMS;
            this.itemListLV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemListLV.FullRowSelect = true;
            this.itemListLV.HideSelection = false;
            this.itemListLV.Location = new System.Drawing.Point(0, 0);
            this.itemListLV.Name = "itemListLV";
            this.itemListLV.Size = new System.Drawing.Size(1006, 529);
            this.itemListLV.TabIndex = 1;
            this.itemListLV.UseCompatibleStateImageBehavior = false;
            this.itemListLV.View = System.Windows.Forms.View.Details;
            this.itemListLV.DragDrop += new System.Windows.Forms.DragEventHandler(this.itemListLV_DragDrop);
            this.itemListLV.DragEnter += new System.Windows.Forms.DragEventHandler(this.itemListLV_DragEnter);
            this.itemListLV.KeyDown += new System.Windows.Forms.KeyEventHandler(this.itemListLV_KeyDown);
            this.itemListLV.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.itemListLV_MouseDoubleClick);
            // 
            // idCH
            // 
            this.idCH.Text = "#Id";
            this.idCH.Width = 24;
            // 
            // nameCH
            // 
            this.nameCH.Text = "Name";
            this.nameCH.Width = 256;
            // 
            // stateCH
            // 
            this.stateCH.Text = "State";
            this.stateCH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.stateCH.Width = 128;
            // 
            // progressCH
            // 
            this.progressCH.Text = "Progress";
            this.progressCH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.progressCH.Width = 196;
            // 
            // speedCH
            // 
            this.speedCH.Text = "Speed";
            this.speedCH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.speedCH.Width = 96;
            // 
            // dirCH
            // 
            this.dirCH.Text = "Download directory";
            this.dirCH.Width = 128;
            // 
            // fileCH
            // 
            this.fileCH.Text = "File name";
            this.fileCH.Width = 128;
            // 
            // uriCH
            // 
            this.uriCH.Text = "Direct link";
            this.uriCH.Width = 256;
            // 
            // downloadLinkCH
            // 
            this.downloadLinkCH.Text = "Indirect link";
            this.downloadLinkCH.Width = 256;
            // 
            // itemListCMS
            // 
            this.itemListCMS.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.itemListCMS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openTSMI,
            this.browseDirectoryTSMI,
            this.toolStripSeparator5,
            this.checkAllTSMI,
            this.uncheckAllTSMI});
            this.itemListCMS.Name = "itemListCMS";
            this.itemListCMS.ShowImageMargin = false;
            this.itemListCMS.Size = new System.Drawing.Size(189, 114);
            this.itemListCMS.Opening += new System.ComponentModel.CancelEventHandler(this.itemListCMS_Opening);
            // 
            // openTSMI
            // 
            this.openTSMI.Name = "openTSMI";
            this.openTSMI.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.openTSMI.Size = new System.Drawing.Size(188, 26);
            this.openTSMI.Text = "Open";
            this.openTSMI.Click += new System.EventHandler(this.openTSMI_Click);
            // 
            // browseDirectoryTSMI
            // 
            this.browseDirectoryTSMI.Name = "browseDirectoryTSMI";
            this.browseDirectoryTSMI.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.browseDirectoryTSMI.Size = new System.Drawing.Size(188, 26);
            this.browseDirectoryTSMI.Text = "Browse directory";
            this.browseDirectoryTSMI.Click += new System.EventHandler(this.browseDirectoryTSMI_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(185, 6);
            // 
            // checkAllTSMI
            // 
            this.checkAllTSMI.Name = "checkAllTSMI";
            this.checkAllTSMI.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.A)));
            this.checkAllTSMI.Size = new System.Drawing.Size(188, 26);
            this.checkAllTSMI.Text = "Check all";
            this.checkAllTSMI.Click += new System.EventHandler(this.checkAllTSMI_Click);
            // 
            // uncheckAllTSMI
            // 
            this.uncheckAllTSMI.Name = "uncheckAllTSMI";
            this.uncheckAllTSMI.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Z)));
            this.uncheckAllTSMI.Size = new System.Drawing.Size(188, 26);
            this.uncheckAllTSMI.Text = "Uncheck all";
            this.uncheckAllTSMI.Click += new System.EventHandler(this.uncheckAllTSMI_Click);
            // 
            // updateTmr
            // 
            this.updateTmr.Enabled = true;
            this.updateTmr.Interval = 500;
            this.updateTmr.Tick += new System.EventHandler(this.updateTmr_Tick);
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "fetchdb";
            this.openFileDialog.FileName = "fetcherDB.fetchdb";
            this.openFileDialog.Filter = "Fetcher Database(*.fetchdb)|*.fetchdb|Plain text (*.txt)|*.txt|All files(*.*)|*.*" +
    "";
            this.openFileDialog.SupportMultiDottedExtensions = true;
            this.openFileDialog.Title = "Choose Fetcher Database file";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "fetchdb";
            this.saveFileDialog.FileName = "fetcherDB.fetchdb";
            this.saveFileDialog.Filter = "Fetcher Database(*.fetchdb)|*.fetchdb|Plain text (*.txt)|*.txt";
            this.saveFileDialog.SupportMultiDottedExtensions = true;
            this.saveFileDialog.Title = "Choose file name to save the database";
            // 
            // helpTSDDB
            // 
            this.helpTSDDB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.helpTSDDB.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpTSMI,
            this.toolStripSeparator6,
            this.aboutTSMI});
            this.helpTSDDB.Image = ((System.Drawing.Image)(resources.GetObject("helpTSDDB.Image")));
            this.helpTSDDB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.helpTSDDB.Name = "helpTSDDB";
            this.helpTSDDB.Size = new System.Drawing.Size(34, 24);
            this.helpTSDDB.Text = "Help";
            this.helpTSDDB.ToolTipText = "Click to get help";
            // 
            // aboutTSMI
            // 
            this.aboutTSMI.Name = "aboutTSMI";
            this.aboutTSMI.Size = new System.Drawing.Size(181, 26);
            this.aboutTSMI.Text = "About";
            this.aboutTSMI.Click += new System.EventHandler(this.aboutTSMI_Click);
            // 
            // helpTSMI
            // 
            this.helpTSMI.Name = "helpTSMI";
            this.helpTSMI.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.helpTSMI.Size = new System.Drawing.Size(181, 26);
            this.helpTSMI.Text = "Help";
            this.helpTSMI.Click += new System.EventHandler(this.helpTSMI_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(178, 6);
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 555);
            this.Controls.Add(this.itemListLV);
            this.Controls.Add(this.statusStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "MainFrm";
            this.Text = "Fetcher";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFrm_FormClosing);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.itemListCMS.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripDropDownButton dataBaseTSDDB;
        private System.Windows.Forms.ToolStripMenuItem openDatabaseTSMI;
        private System.Windows.Forms.ToolStripMenuItem closeDatabaseTSMI;
        private System.Windows.Forms.ToolStripSplitButton downloadTSSB;
        private System.Windows.Forms.ToolStripMenuItem downloadAllTSMI;
        private System.Windows.Forms.ToolStripDropDownButton itemsTSDDB;
        private System.Windows.Forms.ToolStripMenuItem addTSMI;
        private System.Windows.Forms.ToolStripMenuItem removeTSMI;
        private System.Windows.Forms.ToolStripStatusLabel statusTSSL;
        private System.Windows.Forms.ToolStripProgressBar progressTSPB;
        private System.Windows.Forms.ColumnHeader idCH;
        private System.Windows.Forms.ColumnHeader nameCH;
        private System.Windows.Forms.ColumnHeader stateCH;
        private System.Windows.Forms.ColumnHeader dirCH;
        protected internal System.Windows.Forms.ListView itemListLV;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem settingsDatabaseTSMI;
        private System.Windows.Forms.ToolStripMenuItem downloadAgainToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearAllDownloadsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveDatabaseTSMI;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Timer updateTmr;
        private System.Windows.Forms.ColumnHeader uriCH;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripSplitButton stopDownloadingTSSB;
        private System.Windows.Forms.ToolStripMenuItem stopAllTSMI;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ContextMenuStrip itemListCMS;
        private System.Windows.Forms.ToolStripMenuItem openTSMI;
        private System.Windows.Forms.ToolStripMenuItem browseDirectoryTSMI;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem checkAllTSMI;
        private System.Windows.Forms.ToolStripMenuItem uncheckAllTSMI;
        private System.Windows.Forms.ColumnHeader progressCH;
        private System.Windows.Forms.ColumnHeader speedCH;
        private System.Windows.Forms.ColumnHeader fileCH;
        private System.Windows.Forms.ColumnHeader downloadLinkCH;
        private System.Windows.Forms.ToolStripDropDownButton helpTSDDB;
        private System.Windows.Forms.ToolStripMenuItem helpTSMI;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem aboutTSMI;
    }
}


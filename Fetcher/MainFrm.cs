﻿using Microsoft.WindowsAPICodePack.Taskbar;
using MS.WindowsAPICodePack.Internal;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Fetcher
{
    public partial class MainFrm : Form
    {
        public FetchItemCollection Database;

        private int _downloadsCount, _totalProgress;
        private long _totalSpeed, _totalReceived, _totalToReceive;
        private TaskbarManager winTaskbar;
        private Queue<FetchItem> _downloadQueue;
        private List<FetchItem> _downloadPool;

        public MainFrm()
        {
            // If Windows supports DWM enables some cool features
            if (!CoreHelpers.RunningOnXP)
                // Get Windows Taskbar Manager instance
                winTaskbar = TaskbarManager.Instance;
            else
                winTaskbar = null;

            InitializeComponent();

            // Initialize default database
            InitDatabase();

            // Zero counters
            _downloadsCount = 0;
            _totalProgress = 0;
            _totalReceived = 0;
            _totalToReceive = 0;
            _totalSpeed = 0;
        }

        /// <summary>
        /// Adds a fetch item to the ListView
        /// </summary>
        /// <param name="fetchItem">Fetch item to add</param>
        /// <returns>Added ListView item</returns>
        public ListViewItem AddItemToListView(FetchItem fetchItem)
        {
            /*lock (itemListLV)
            {*/
                string[] cols = new string[9];

                cols[0] = Database.IndexOf(fetchItem).ToString();   // Index in database collection
                cols[1] = fetchItem.ItemName;                       // Item name
                cols[2] = fetchItem.StateString;                    // Item state
                cols[3] = string.Empty;                             // Download progress
                cols[4] = string.Empty;                             // Download speed
                cols[5] = fetchItem.Directory;                      // Download directory
                cols[6] = string.Empty;                             // File name
                cols[7] = string.Empty;                             // Direct download link
                cols[8] = fetchItem.DownloadLink;                   // Indirect download link

                if (fetchItem.Category == string.Empty)
                    return itemListLV.Items.Add(new ListViewItem(cols));
                else
                {
                    ListViewGroup lvg = GetListViewGroup(itemListLV, fetchItem.Category);

                    if (lvg == null)
                    {
                        lvg = new ListViewGroup(fetchItem.Category);
                        itemListLV.Groups.Add(lvg);
                    }

                    return itemListLV.Items.Add(new ListViewItem(cols, lvg));
                }
            //}
        }

        /// <summary>
        /// Checks if item is busy
        /// </summary>
        /// <param name="item">Item to check</param>
        /// <returns>Returns True if it busy, otherwise - False</returns>
        private bool IsItemBusy(FetchItem item)
        {
            return (item.State == FetchItemState.Downloading ||
                    item.State == FetchItemState.Initializing ||
                    item.State == FetchItemState.Ready);
        }

        /// <summary>
        /// Updates the item list
        /// </summary>
        private void UpdateLV()
        {
            int idx, totalProgress = 0;

            // Zero counters
            _totalSpeed = _totalReceived = _totalToReceive = 0;
            _downloadsCount = 0;

            /*lock (itemListLV)
            {*/
                itemListLV.BeginUpdate();

                foreach (ListViewItem lvi in itemListLV.Items)
                {
                    idx = int.Parse(lvi.SubItems[0].Text);

                    // WTF!? In case of "something went wrong..."
                    if (idx < 0 || Database.Count <= idx)
                        continue;

                    // Update "State" column
                    lvi.SubItems[2].Text = Database[idx].StateString;

                    // Update next columns
                    if (Database[idx].DownloadInfo != null)
                    {
                        // Update "Progress" column
                        lvi.SubItems[3].Text = string.Format("{0}% | {1}/{2}",
                                                             Database[idx].DownloadInfo.Progress,
                                                             Program.GetSizeString(Database[idx].DownloadInfo.ReceivedBytes),
                                                             Program.GetSizeString(Database[idx].DownloadInfo.TotalBytes));

                        if (Database[idx].State == FetchItemState.Downloading)
                        {
                            _downloadsCount++;
                            totalProgress += Database[idx].DownloadInfo.Progress;
                        }

                        _totalReceived += Database[idx].DownloadInfo.ReceivedBytes;
                        _totalToReceive += Database[idx].DownloadInfo.TotalBytes;


                        // Update "Speed" column
                        if (Database[idx].DownloadInfo.CurrentSpeed != 0)
                        {
                            lvi.SubItems[4].Text = string.Format("{0}/s",
                                                             Program.GetSizeString(Database[idx].DownloadInfo.CurrentSpeed));

                            _totalSpeed += Database[idx].DownloadInfo.CurrentSpeed;
                        }
                        else
                            lvi.SubItems[4].Text = string.Empty;

                        // Update "File name" column
                        lvi.SubItems[6].Text = Database[idx].DownloadInfo.FileName;

                        // Update "Direct link" column
                        lvi.SubItems[7].Text = Database[idx].DownloadInfo.Url.OriginalString;
                    }
                }

                itemListLV.EndUpdate();
            //}

            _totalProgress = (_downloadsCount > 0) ? (int)Math.Round((double)totalProgress / _downloadsCount) : 0;
        }

        /// <summary>
        /// Updates status label and overall progress bar
        /// </summary>
        private void UpdateOverallStatus()
        {
            if (_downloadsCount == 0 && _downloadPool.Count == 0)
            {
                statusTSSL.Text = "Ready";

                if (winTaskbar != null)
                {
                    winTaskbar.SetProgressState(TaskbarProgressBarState.NoProgress);
                    winTaskbar.SetOverlayIcon(null, null);
                }

                stopDownloadingTSSB.Visible = false;
                settingsDatabaseTSMI.Visible = true;
                openDatabaseTSMI.Visible = true;
            }
            else if (_downloadsCount == 0 && _downloadPool.Count > 0)
            {
                statusTSSL.Text = "Initializing";

                if (winTaskbar != null)
                {
                    winTaskbar.SetProgressState(TaskbarProgressBarState.Indeterminate);
                    winTaskbar.SetOverlayIcon(null, null);
                }

                stopDownloadingTSSB.Visible = true;
                settingsDatabaseTSMI.Visible = false;
                openDatabaseTSMI.Visible = false;
            }
            else if (_downloadsCount > 0 && _downloadPool.Count == 0)
            {
                statusTSSL.Text = "Stopping";

                if (winTaskbar != null)
                {
                    winTaskbar.SetProgressState(TaskbarProgressBarState.Paused);
                    winTaskbar.SetOverlayIcon(null, null);
                }

                stopDownloadingTSSB.Visible = true;
                settingsDatabaseTSMI.Visible = true;
                openDatabaseTSMI.Visible = true;
            }
            else if (_downloadsCount > 0 && _downloadPool.Count > 0)
            {
                statusTSSL.Text = string.Format("Downloads: {0} Progress: {1}% | {2}/{3} Speed: {4}/s", 
                                                _downloadsCount, 
                                                _totalProgress,
                                                Program.GetSizeString(_totalReceived),
                                                Program.GetSizeString(_totalToReceive),
                                                Program.GetSizeString(_totalSpeed));

                if (winTaskbar != null)
                {
                    winTaskbar.SetProgressState(TaskbarProgressBarState.Normal);
                    winTaskbar.SetOverlayIcon(Properties.Resources.download, statusTSSL.Text);
                }
                stopDownloadingTSSB.Visible = true;
                settingsDatabaseTSMI.Visible = false;
                openDatabaseTSMI.Visible = false;
            }
            else
            {
                statusTSSL.Text = "Error: unknow state";

                if (winTaskbar != null)
                {
                    winTaskbar.SetProgressState(TaskbarProgressBarState.Error);
                    winTaskbar.SetOverlayIcon(null, null);
                }
            }

            progressTSPB.Value = _totalProgress;

            if (winTaskbar != null)
                winTaskbar.SetProgressValue(_totalProgress, 100);
        }

        /// <summary>
        /// Initializes default database
        /// </summary>
        private void InitDatabase()
        {
            Database = new FetchItemCollection()
            {
                Name = Program.DEFAULT_DB_NAME,
                DefaultDirectory = Program.DEFAULT_DL_DIR,
                ThreadCount = Program.DEFAULT_THREAD_COUNT
            };

            UpdateConfig();
        }

        /// <summary>
        /// Updates application configuration to work with new settings
        /// </summary>
        private void UpdateConfig()
        {
            // Setup item list
            itemListLV.Items.Clear();

            foreach (FetchItem fetchItem in Database)
                AddItemToListView(fetchItem).Checked = true;

            // Initialize download queue and pool
            _downloadQueue = new Queue<FetchItem>();
            _downloadPool = new List<FetchItem>(Database.ThreadCount);

            // Check default directory existence
            if (!Directory.Exists(Database.DefaultDirectory))
                Directory.CreateDirectory(Database.DefaultDirectory);

            Text = string.Format("{0} – {1}", Database.Name, ProductName);
        }

        /// <summary>
        /// Handles the download pool
        /// </summary>
        private void CheckPool()
        {
            int i = 0;

            // Removing downloaded items from the download pool
            while (i < _downloadPool.Count)
            {
                if (!IsItemBusy(_downloadPool[i]))
                {
                    _downloadPool.RemoveAt(i);
                    i = 0;
                }
                else
                    i++;
            }

            // Add items to download if they are exist
            while (_downloadQueue.Count > 0 && _downloadPool.Count < Database.ThreadCount)
            {
                FetchItem fetchItem = _downloadQueue.Dequeue();

                // If fetch item disposed, create new
                if (fetchItem.IsDisposed || fetchItem.State == FetchItemState.Ready)
                    fetchItem = new FetchItem(
                                                fetchItem.ToString(Program.SERIALIZATION_SEPARATOR), 
                                                Program.SERIALIZATION_SEPARATOR
                                             );
                
                // Start download
                fetchItem.StartDownload();
                _downloadPool.Add(fetchItem);
            }
        }

        /// <summary>
        /// Returns group with specified header if it exists in listView
        /// </summary>
        /// <param name="listView">ListView control</param>
        /// <param name="header">Group header</param>
        /// <returns>Returns link to the ListViewGroup if it exists, otherwise returns null</returns>
        private ListViewGroup GetListViewGroup(ListView listView, string header)
        {
            foreach (ListViewGroup lvg in listView.Groups)
            {
                if (lvg.Header == header)
                    return lvg;
            }
            return null;
        }

        /// <summary>
        /// Loads database
        /// </summary>
        /// <param name="fileName">Database file name</param>
        private void LoadDatabase(string fileName)
        {
            closeDatabaseTSMI_Click(this, new EventArgs());
            if (Database.Count == 0)
            {
                Database.Load(fileName);
                UpdateConfig();
            }
        }

        #region GUI
        private void addTSMI_Click(object sender, EventArgs e)
        {
            ItemCreationFrm itemCreationFrm = new ItemCreationFrm();
            itemCreationFrm.Show(this);
        }

        private void settingsDatabaseTSMI_Click(object sender, EventArgs e)
        {
            DBSettingsFrm dbSettingsFrm = new DBSettingsFrm();
            if (dbSettingsFrm.ShowDialog(this) == DialogResult.OK)
                UpdateConfig();
        }

        private void closeDatabaseTSMI_Click(object sender, EventArgs e)
        {
            if (Database.Count > 0)
            {
                if (MessageBox.Show(
                                    "Are you sure want to close current database? Unsaved changes will be lost!",
                                    "Database closing",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Warning
                                   ) == DialogResult.Yes)
                    InitDatabase();
            }
            else
                InitDatabase();
        }

        private void openDatabaseTSMI_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
                LoadDatabase(openFileDialog.FileName);
        }

        private void saveDatabaseTSMI_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
                Database.Save(saveFileDialog.FileName);
        }

        private void removeTSMI_Click(object sender, EventArgs e)
        {
            /*lock (itemListLV)
            {*/
                if (itemListLV.SelectedItems.Count > 0)
                {
                    // Temporary index
                    int idx;

                    if (MessageBox.Show("Delete downloaded files?",
                                        "Removing fetch items",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        foreach (ListViewItem lvi in itemListLV.SelectedItems)
                        {
                            idx = int.Parse(lvi.SubItems[0].Text);

                            if (!IsItemBusy(Database[idx]) && Database[idx].DownloadInfo != null)
                                try { File.Delete(Database[idx].DownloadInfo.SavePath); }
                                catch (ArgumentException) { }
                        }
                    }

                    for (int i = itemListLV.SelectedItems.Count - 1; i >= 0; i--)
                    {
                        idx = int.Parse(itemListLV.SelectedItems[i].SubItems[0].Text);

                        if (!IsItemBusy(Database[idx]))
                        {
                            Database.RemoveAt(idx);
                            itemListLV.Items.Remove(itemListLV.SelectedItems[i]);
                        }
                    }
                }
            //}
        }

        private void downloadTSSB_ButtonClick(object sender, EventArgs e)
        {
            // Temporary index
            int idx;

            foreach (ListViewItem lvi in itemListLV.CheckedItems)
            {
                idx = int.Parse(lvi.SubItems[0].Text);
                if (Database[idx].State == FetchItemState.NotInitialized || 
                    Database[idx].State == FetchItemState.Stopped)
                    _downloadQueue.Enqueue(Database[idx]);
            }
        }

        private void downloadAllTSMI_Click(object sender, EventArgs e)
        {
            foreach (FetchItem fetchItem in Database)
            {
                if (fetchItem.State == FetchItemState.NotInitialized ||
                    fetchItem.State == FetchItemState.Stopped)
                    _downloadQueue.Enqueue(fetchItem);
            }
        }

        private void downloadAgainToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Temporary index
            int idx;

            foreach (ListViewItem lvi in itemListLV.CheckedItems)
            {
                idx = int.Parse(lvi.SubItems[0].Text);
                if (Database[idx].State == FetchItemState.NotInitialized ||
                    Database[idx].State == FetchItemState.Stopped ||
                    Database[idx].State == FetchItemState.Done)
                    _downloadQueue.Enqueue(Database[idx]);
            }
        }

        private void clearAllDownloadsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure want to delete ALL downloaded files?",
                                "Clearing download files",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (stopDownloadingTSSB.Visible)
                    stopAllTSMI_Click(sender, e);

                foreach (FetchItem fetchItem in Database)
                {
                    if (!IsItemBusy(fetchItem) && fetchItem.DownloadInfo != null)
                        try { File.Delete(fetchItem.DownloadInfo.SavePath); }
                        catch (ArgumentException) { }
                }
            }
        }

        private void stopDownloadingTSSB_ButtonClick(object sender, EventArgs e)
        {
            // Temporary index
            int idx;

            if (MessageBox.Show(
                                "Are you sure want to stop selected downloads?",
                                "Aborting selected downloads",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question
                               ) == DialogResult.Yes)

                foreach (ListViewItem lvi in itemListLV.CheckedItems)
                {
                    idx = int.Parse(lvi.SubItems[0].Text);
                    if (_downloadQueue.Contains(Database[idx]))
                        lock (_downloadQueue)
                        {
                            _downloadQueue = new Queue<FetchItem>(_downloadQueue.Where
                                (
                                    new Func<FetchItem, bool>   (
                                                                    (item) => item != Database[idx]
                                                                )
                                ));
                            /*List<FetchItem> tmpLst = new List<FetchItem>(_downloadQueue);
                            tmpLst.Remove(Database[idx]);
                            _downloadQueue = new Queue<FetchItem>(tmpLst);*/
                        }
                    else if (_downloadPool.Contains(Database[idx]))
                        lock (_downloadPool)
                        {
                            _downloadPool.Remove(Database[idx]);
                        }

                    Database[idx].StopDownloading();
                }
        }

        private void stopAllTSMI_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(
                                "Are you sure want to stop ALL downloads?",
                                "Aborting all downloads",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question
                               ) == DialogResult.Yes)
            {
                lock (_downloadQueue) lock (_downloadPool)
                    {
                        _downloadQueue.Clear();
                        _downloadPool.Clear();
                    }

                foreach (FetchItem fetchItem in Database)
                    fetchItem.StopDownloading();
            }
        }

        private void itemListLV_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
                /*lock (itemListLV)
                {*/
                    foreach (ListViewItem lvi in itemListLV.Items)
                        lvi.Selected = true;
                //}
        }

        private void itemListLV_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            openTSMI_Click(sender, e);
        }

        private void itemListLV_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void itemListLV_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            LoadDatabase(files[0]);
        }

        private void checkAllTSMI_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in itemListLV.Items)
                lvi.Checked = true;
        }

        private void uncheckAllTSMI_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in itemListLV.Items)
                lvi.Checked = false;
        }

        private void itemListCMS_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            browseDirectoryTSMI.Enabled = openTSMI.Enabled = itemListLV.SelectedItems.Count > 0;
        }

        private void itemsTSDDB_DropDownOpening(object sender, EventArgs e)
        {
            removeTSMI.Enabled = itemListLV.SelectedItems.Count > 0;
        }

        private void downloadTSSB_DropDownOpening(object sender, EventArgs e)
        {
            downloadAgainToolStripMenuItem.Enabled = itemListLV.CheckedItems.Count > 0;
        }

        private void MainFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (FetchItem fetchItem in Database)
                foreach (Control ctrl in fetchItem.Controls)
                {
                    ctrl.Dispose();
                }
        }

        private void aboutTSMI_Click(object sender, EventArgs e)
        {
            AboutDialog aboutDialog = new AboutDialog();
            aboutDialog.ShowDialog();
        }

        private void helpTSMI_Click(object sender, EventArgs e)
        {
            Process.Start("https://bitbucket.org/random_rage/fetcher/wiki/Home");
        }

        private void openTSMI_Click(object sender, EventArgs e)
        {
            if (itemListLV.SelectedItems.Count > 0)
            {
                int idx = int.Parse(itemListLV.SelectedItems[0].SubItems[0].Text);

                if (!IsItemBusy(Database[idx]) && Database[idx].DownloadInfo != null)
                {
                    /*if (Database[idx].Torrent)
                    {
                        if (Database[idx].DownloadInfo.CanOpenDownloadedFileFolder())
                            Database[idx].DownloadInfo.OpenDownloadedFileFolder();
                    }
                    else
                    {*/
                        if (Database[idx].DownloadInfo.CanOpenDownloadedFile())
                            Database[idx].DownloadInfo.OpenDownloadedFile();
                    //}
                }
            }
        }

        private void browseDirectoryTSMI_Click(object sender, EventArgs e)
        {
            if (itemListLV.SelectedItems.Count > 0)
            {
                int idx = int.Parse(itemListLV.SelectedItems[0].SubItems[0].Text);

                if (!IsItemBusy(Database[idx]) && Database[idx].DownloadInfo != null)
                {
                    if (Database[idx].DownloadInfo.CanOpenDownloadedFileFolder())
                        Database[idx].DownloadInfo.OpenDownloadedFileFolder();
                }
            }
        }

        private void updateTmr_Tick(object sender, EventArgs e)
        {
            CheckPool();
            UpdateLV();
            UpdateOverallStatus();
        } 
        #endregion
    }
}

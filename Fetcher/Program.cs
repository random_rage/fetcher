﻿using System;
using System.Windows.Forms;

namespace Fetcher
{
    static class Program
    {
        public const char SERIALIZATION_SEPARATOR = '▒';

        public const int WEB_SPIDER_DOM_WAITING_MAX_ATTEMPTS = 50;
        public const int WEB_SPIDER_DOM_WAITING_TIMEOUT = 100;
        public const int WEB_SPIDER_SEARCH_TIMEOUT = 100;

        public const string DEFAULT_DB_NAME = "Default";
        public const string DEFAULT_DL_DIR = "Downloads";
        public const byte DEFAULT_THREAD_COUNT = 2;
        
        private static readonly string[] SizeSuffixes = { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainFrm());
        }

        /// <summary>
        /// Gets formatted string that indicates data size
        /// </summary>
        /// <param name="value">Data size in bytes</param>
        /// <returns>Returns formatted string that indicates data size</returns>
        public static string GetSizeString(long value)
        {
            if (value < 0) { return "-" + GetSizeString(-value); }
            if (value == 0) { return "0" + SizeSuffixes[0]; }

            int mag = (int)Math.Log(value, 1024);
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }

        /// <summary>
        /// Shows an error message box with one button - "OK"
        /// </summary>
        /// <param name="text">Text in the message box</param>
        /// <param name="caption">Title of the message box</param>
        public static void ShowError(string text, string caption)
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
